import 'package:flutter/material.dart';
import 'package:project_app/services/auth.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:project_app/pages/map_page.dart';

class HomePage extends StatefulWidget {
  final String deviceid;
  HomePage({Key key, @required this.deviceid}) : super(key: key);

  @override
  State<StatefulWidget> createState() => new _HomePageState();
}

class _HomePageState extends State<HomePage> {
  static final databaseReference = FirebaseDatabase.instance.reference();

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        appBar: new AppBar(
          title: new Text('Project App : ' + widget.deviceid),

        ),
        body: ListView(
          shrinkWrap: true,
          children: <Widget>[
            gpsButton(),
            lockButton(),
            unlockButton(),
            startButton()
          ],
        ));
  }

  Widget gpsButton() {
    return Padding(
        padding: EdgeInsets.fromLTRB(0.0, 45.0, 0.0, 0.0),
        child: SizedBox(
          height: 40.0,
          child: RaisedButton(
            elevation: 5.0,
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(30.0)),
            color: Colors.blue,
            child: Text('GPS View',
                style: TextStyle(fontSize: 20.0, color: Colors.white)),
            onPressed: () {
              Navigator.push(context, MaterialPageRoute(builder: (context) {
                return new MapPage(deviceid: widget.deviceid);
              }));
            },
          ),
        ));
  }

  Widget lockButton() {
    return Padding(
        padding: EdgeInsets.fromLTRB(0.0, 45.0, 0.0, 0.0),
        child: SizedBox(
          height: 40.0,
          child: RaisedButton(
            elevation: 5.0,
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(30.0)),
            color: Colors.blue,
            child: Text('ล็อครถ',
                style: TextStyle(fontSize: 20.0, color: Colors.white)),
            onPressed: () {
              databaseReference.child(widget.deviceid).update({'lock': 1});
            },
          ),
        ));
  }

  Widget unlockButton() {
    return Padding(
        padding: EdgeInsets.fromLTRB(0.0, 45.0, 0.0, 0.0),
        child: SizedBox(
          height: 40.0,
          child: RaisedButton(
            elevation: 5.0,
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(30.0)),
            color: Colors.blue,
            child: Text('ปลดล็อค',
                style: TextStyle(fontSize: 20.0, color: Colors.white)),
            onPressed: () {
              databaseReference.child(widget.deviceid).update({'lock': 0});
            },
          ),
        ));
  }

  Widget startButton() {
    return Padding(
        padding: EdgeInsets.fromLTRB(0.0, 45.0, 0.0, 0.0),
        child: SizedBox(
          height: 40.0,
          child: RaisedButton(
            elevation: 5.0,
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(30.0)),
            color: Colors.blue,
            child: Text('สตารท์รถ',
                style: TextStyle(fontSize: 20.0, color: Colors.white)),
            onPressed: () {
              databaseReference.child(widget.deviceid).update({'start': 1});
            },
          ),
        ));
  }
}

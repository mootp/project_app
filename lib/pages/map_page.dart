import 'dart:async';

import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:firebase_database/firebase_database.dart';

class MapPage extends StatefulWidget {
  final String deviceid;
  MapPage({Key key, @required this.deviceid}) : super(key: key);

  @override
  _MapPageState createState() => _MapPageState();
}

class _MapPageState extends State<MapPage> {
  static double currentLatitude = 0.0;
  static double currentLongitude = 0.0;
  static GoogleMapController mapController;
  StreamSubscription subscription;
  Map<String, double> currentLocation = new Map();
  Map<MarkerId, Marker> markers = <MarkerId, Marker>{};

  setupMap() {
    try {
      subscription = FirebaseDatabase.instance
          .reference()
          .child(widget.deviceid)
          .onValue
          .listen((event) {
            
        mapController.animateCamera(
          CameraUpdate.newCameraPosition(
            CameraPosition(
                target: LatLng(event.snapshot.value['latitude'],
                    event.snapshot.value['longitude']),
                zoom: 15),
          ),
        );

        Marker marker = Marker(
          markerId: MarkerId(widget.deviceid),
          icon: BitmapDescriptor.defaultMarker,
          position: LatLng(event.snapshot.value['latitude'],
              event.snapshot.value['longitude']),
        );

        setState(() {
          currentLatitude = event.snapshot.value['latitude'];
          currentLongitude = event.snapshot.value['longitude'];
          markers[MarkerId(widget.deviceid)] = marker;
        });
      });
    } catch (e) {
      print(e);
    }
  }

  @override
  void dispose() {
    subscription.cancel();
    super.dispose();
  }

  @override
  void initState() {
    super.initState();
    setupMap();
  }

  void _onMapCreated(GoogleMapController controller) {
    setState(() {
      mapController = controller;
    });
  }

  @override
  Widget build(BuildContext context) {
    double h = MediaQuery.of(context).size.height;

    return Scaffold(
        appBar: AppBar(title: const Text('GPS View')),
        body: Padding(
          padding: EdgeInsets.all(0),
          child: Column(
            children: <Widget>[
              Container(
                child: SizedBox(
                  width: double.infinity,
                  height: h * 0.8,
                  child: GoogleMap(
                    initialCameraPosition: CameraPosition(
                        target: LatLng(currentLatitude, currentLongitude),
                        zoom: 17),
                    onMapCreated: _onMapCreated,
                    markers: Set<Marker>.of(markers.values),
                  ),
                ),
              ),
              Container(
                child: Text('Lat/Lng: $currentLatitude/$currentLongitude'),
              ),
              Text("User ID: ${widget.deviceid}")
            ],
          ),
        ));
  }
}
